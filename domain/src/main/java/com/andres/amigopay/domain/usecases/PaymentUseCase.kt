package com.andres.amigopay.domain.usecases

import com.andres.amigopay.domain.entity.PaymentResponse
import com.andres.amigopay.domain.repository.PaymentRepository
import com.andres.amigopay.payment.model.PaymentInfo
import io.reactivex.observers.DisposableObserver
import lat.adev.igneous.domain.executor.AppExecutor
import lat.adev.igneous.domain.executor.PostExecutionThread

class PaymentUseCase constructor(
    appExecutor: AppExecutor,
    postExecutionThread: PostExecutionThread,
    var paymentRepository: PaymentRepository)
    : UseCase(appExecutor, postExecutionThread){

    fun getPaymentInfo(token: String, observer: DisposableObserver<PaymentInfo>){
        execute(paymentRepository.getPaymentInfo(token), observer)
    }

    fun sendPaymentTransaction(token: String, transactionCode: String, observer: DisposableObserver<PaymentResponse>){
        execute(paymentRepository.sendPaymentTransaction(token, transactionCode), observer)
    }

}
