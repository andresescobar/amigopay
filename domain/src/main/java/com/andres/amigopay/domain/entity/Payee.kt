package com.andres.amigopay.payment.model

data class Payee(var payeeCode: String, var companyName: String)