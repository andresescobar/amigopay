package com.andres.amigopay.payment.model

import java.math.BigDecimal

data class PaymentInfo(var payee: Payee
                       , var payer: Payer
                       , var amount: BigDecimal
                       , var currency: String)