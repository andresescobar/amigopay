package com.andres.amigopay.payment.model

data class Payer(var creditCompany: String
                 , var cardNumber: String
                 , var cardholderName: String
                 , var expirationDate: String
                 , var securityCode: String)