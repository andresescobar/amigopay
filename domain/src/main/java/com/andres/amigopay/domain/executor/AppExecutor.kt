package lat.adev.igneous.domain.executor

import java.util.concurrent.Executor

interface AppExecutor : Executor
