package com.andres.amigopay.domain.usecases

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import lat.adev.igneous.domain.executor.AppExecutor
import lat.adev.igneous.domain.executor.PostExecutionThread

abstract class UseCase
internal constructor(private val appExecutor: AppExecutor
                    , private val postExecutionThread: PostExecutionThread) {
    private val disposables: CompositeDisposable = CompositeDisposable()

    internal fun <T> execute(observable: Observable<T>, observer: DisposableObserver<T>) {
        val disposable = observable
                .subscribeOn(Schedulers.from(appExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(observer)

        addDisposable(disposable)
    }

    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    private fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

}
