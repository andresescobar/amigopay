package com.andres.amigopay.domain.repository

import com.andres.amigopay.domain.entity.PaymentResponse
import com.andres.amigopay.payment.model.PaymentInfo
import io.reactivex.Observable

interface PaymentRepository {

    fun getPaymentInfo(token: String) : Observable<PaymentInfo>

    fun sendPaymentTransaction(token: String, transactionCode: String) : Observable<PaymentResponse>

}