package com.andres.amigopay.domain.entity

data class PaymentResponse(var statusCode : String, var message: String)