package com.andres.amigopay.payment.viewmodel

import android.arch.lifecycle.ViewModel
import com.andres.amigopay.domain.entity.PaymentResponse
import com.andres.amigopay.domain.observer.BaseObserver
import com.andres.amigopay.domain.usecases.PaymentUseCase
import com.andres.amigopay.payment.model.PaymentInfo
import android.arch.lifecycle.MutableLiveData


class PaymentViewModel(var paymentUseCase: PaymentUseCase) : ViewModel(){

    //Live data

    val paymentInfo: MutableLiveData<PaymentInfo> by lazy {
        MutableLiveData<PaymentInfo>()
    }

    val paymentResponse: MutableLiveData<PaymentResponse> by lazy {
        MutableLiveData<PaymentResponse>()
    }

    //funciones

    fun getPaymentInfo(token: String, observer : BaseObserver<PaymentInfo>){
        paymentUseCase.getPaymentInfo(token, observer)
    }

    fun sendPaymentTransaction(token: String, transactionCode: String, observer: BaseObserver<PaymentResponse>){
        paymentUseCase.sendPaymentTransaction(token, transactionCode, observer)
    }

    //observers

    private inner class GetPaymentInfo : BaseObserver<PaymentInfo>() {

        override fun onNext(response: PaymentInfo) {
            paymentInfo.value = response
        }
    }

    private inner class SendPaymentTransaction : BaseObserver<PaymentResponse>() {

        override fun onNext(response: PaymentResponse) {
            paymentResponse.value = response
        }
    }

}