package com.andres.amigopay.payment.view

import android.arch.lifecycle.Observer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.andres.amigopay.amigopay.R
import com.andres.amigopay.domain.entity.PaymentResponse
import com.andres.amigopay.payment.model.PaymentInfo
import com.andres.amigopay.payment.viewmodel.PaymentViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class PaymentActivity : AppCompatActivity() {

    val paymentViewModel : PaymentViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        paymentViewModel.paymentInfo.observe(this, paymentInfoObserver)
        paymentViewModel.paymentResponse.observe(this, paymentResponseObserver)

    }

    val paymentInfoObserver = Observer<PaymentInfo> {
        //mNameTextView.text = newName
    }

    val paymentResponseObserver = Observer<PaymentResponse> {
        //mNameTextView.text = newName
    }


}
