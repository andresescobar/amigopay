package com.andres.amigopay.amigopay.payment.model

import java.math.BigDecimal

data class PaymentDataModel(var payeeModel: PayeeModel
                            , var payerModel: PayerModel
                            , var amount: BigDecimal
                            , var currency: String)