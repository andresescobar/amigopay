package com.andres.amigopay.amigopay.payment.model

data class PayeeModel(var payeeCode: String, var companyName: String)