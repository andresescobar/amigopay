package com.andres.amigopay.amigopay.payment.model

data class PayerModel(var creditCompany: String
                      , var cardNumber: String
                      , var cardholderName: String
                      , var expirationDate: String
                      , var securityCode: String)