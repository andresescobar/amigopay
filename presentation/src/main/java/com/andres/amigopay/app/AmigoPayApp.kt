package com.andres.amigopay.app

import android.app.Application
import com.andres.amigopay.payment.di.paymentModule
import org.koin.android.ext.android.startKoin

class AmigoPayApp : Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin(this, getModules())
    }

    private fun getModules() = listOf(paymentModule)

}