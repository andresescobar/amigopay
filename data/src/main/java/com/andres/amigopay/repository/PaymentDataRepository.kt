package com.andres.amigopay.repository

import com.andres.amigopay.domain.entity.PaymentResponse
import com.andres.amigopay.domain.repository.PaymentRepository
import com.andres.amigopay.payment.model.Payee
import com.andres.amigopay.payment.model.Payer
import com.andres.amigopay.payment.model.PaymentInfo
import io.reactivex.Observable
import java.math.BigDecimal

class PaymentDataRepository : PaymentRepository {

    override fun getPaymentInfo(token: String) : Observable<PaymentInfo>{
        return Observable.just(PaymentInfo(
            Payee("", ""), Payer("", "", "", "", ""), BigDecimal("100"), "")
        )
    }

    override fun sendPaymentTransaction(token: String, transactionCode: String) : Observable<PaymentResponse>{
        return Observable.just(PaymentResponse(
            "0000", "Transaction OK")
        )
    }

}